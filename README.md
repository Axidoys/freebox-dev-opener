# Freebox dev opener

```shell
poetry.exe run list
```


DOC TEMP :
https://chat.openai.com/chat/1e0e31d9-4767-4540-8bd4-76dbf1ab2b81
https://dev.freebox.fr/sdk/server.html
https://dev.freebox.fr/sdk/os/#
http://mafreebox.freebox.fr/api/v4/fw/redir/

Repo :
https://gitlab.com/Axidoys/freebox-dev-opener

An alternative :
https://github.com/hacf-fr/freebox-api
