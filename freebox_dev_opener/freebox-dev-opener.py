import socket
import click

import requests as requests

from freebox_dev_opener.auth import config


def retrieve_port_forwards(session_token):
    port_fwds = requests.get("http://mafreebox.freebox.fr/api/v8/fw/redir/", headers={"X-Fbx-App-Auth": session_token})

    if not port_fwds.ok:
        print("got an error while fetching the port forwards")
        exit(1)

    return port_fwds.json()["result"]


def create_port_forward(session_token, lan_ip: str, port: int):
    result = requests.post("http://mafreebox.freebox.fr/api/v8/fw/redir/", headers={"X-Fbx-App-Auth": session_token},
                           json={
                               "comment": "auto_fdo",
                               "enabled": False,
                               "lan_ip": lan_ip,
                               "lan_port": port,
                               "wan_port_end": port,
                               "wan_port_start": port,
                               "ip_proto": "tcp",
                           })

    print(result.json())

    if not result.ok:
        print("got an error while creating the port forward")
        exit(1)

    return


def update_port_forward(session_token, pfid, enable):
    result = requests.put(f"http://mafreebox.freebox.fr/api/v8/fw/redir/{pfid}",
                          headers={"X-Fbx-App-Auth": session_token}, json={
            "enabled": enable,
        })

    print(result.json())

    if not result.ok:
        print("got an error while updating the port forward")
        exit(1)

    return


@click.command(name="list")
# @click.option('--count', default=1, help='Number of greetings.')
# @click.option('--name', prompt='Your name',
#               help='The person to greet.')
def listing():
    session_token = config()

    forwards = retrieve_port_forwards(session_token)

    for forward in forwards:
        print(
            f"{forward['id']} - {forward['comment']} - {forward['lan_ip']}:{forward['lan_port']} -> {forward['wan_port_start']}")


# @click.command("open")
# @click.option('--override', default=True, help='If the port forward already exists, override it')
# @click.option('--port', prompt='port to add',
#               help='The port that will be opened on the freebox to this PC')
def open_port(override: bool, port: int):
    session_token = config()

    # get the current ip
    socket_info = socket.gethostbyname_ex(socket.gethostname())
    ips = socket_info[2]
    local_ip = [ip for ip in ips if "192.168." in ip][0]

    # check if the port forward already exists
    forward = None
    pfs = retrieve_port_forwards(session_token)
    for pf in pfs:
        if pf["wan_port_start"] == port and pf["wan_port_end"] == port:
            forward = pf

    # fail if already exists AND override is false
    if forward is not None:
        if not override:
            print("The port forward already exists")
            exit(1)
        else: # override

            if forward["enabled"]:
                print("The port forward already exists and is already enabled")
                exit(0)

            print(forward)
            # TODO handle this case, update the port forward with the new name (auto_fdo), etc.
            update_port_forward(session_token, pfid=forward["id"], enable=True)

    else:
        # create the port forward
        create_port_forward(session_token, local_ip, port)

    # from there, will update/create the port forward

    # if the port forward does not already exist, create it
    if forward is None:
        pass

    # check the id/comment/name of the port forward

    # if already enabled
    # say it and exit(0)

    # enable the port forward


if __name__ == '__main__':
    open_port(override=True, port=8081)

    print("Need to use poetry scripts")
