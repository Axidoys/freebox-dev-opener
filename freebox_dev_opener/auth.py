import hashlib
import hmac
import json
import keyring as keyring
import requests as requests

from freebox_dev_opener.globals import *


def check_api_availibility():
    api_info = requests.get("http://mafreebox.freebox.fr/api_version")
    if not api_info.ok:
        print("no freebox detected")
        exit(1)

    #print(api_info.text)

def create_authorization():
    response = requests.post(
        "http://mafreebox.freebox.fr/api/v8/login/authorize/",
        data=json.dumps({
            "app_id": APP_ID,
            "app_name": APP_NAME,
            "app_version": APP_VERSION,
            "device_name": DEVICE_HOSTNAME
        })
    )

    response_json = response.json()

    if not response.ok:
        print(f"Error while pairing: {response_json['msg']}")
        exit(1)

    # Save the app token and track_id for later use
    app_token = response_json["result"]["app_token"]
    track_id = response_json["result"]["track_id"]

    # Print out the app token and track_id
    print(f"App Token: {app_token}")
    print(f"Track ID: {track_id}")

    return app_token, track_id


def fetch_session_token(password):
    # TODO NOTE: in case of session opening failure, ensure that the box you’re connected is the one you expect by checking the uid returned in the answer.

    response = requests.post(
        "http://mafreebox.freebox.fr/api/v8/login/session/",
        data=json.dumps({
            "app_id": APP_ID,
            "password": password
        })
    )

    response_json = response.json()

    if not response.ok:
        print(f"Error while opening session: {response_json['msg']}")
        return None

    return response_json["result"]["session_token"]


def finish_authorization(track_id):
    response = requests.get(f"http://mafreebox.freebox.fr/api/v8/login/authorize/{track_id}")

    response_json = response.json()

    if not response.ok:
        print(f"Error while finishing authorization: {response_json['msg']}")
        exit(1)

    return response_json["result"]["challenge"]


def get_challenge():
    response = requests.get("http://mafreebox.freebox.fr/api/v8/login/")

    response_json = response.json()

    if not response.ok:
        print(f"Error while getting challenge: {response_json['msg']}")
        exit(1)

    return response_json["result"]["challenge"]

# TODO get permissions, can be done via UI of mafreebox.freebox.fr, or  maybe via API : https://dev.freebox.fr/bugs/task/13235
def config():
    print("configuring")

    # TODO get the current ip needed ?

    check_api_availibility()

    app_token = keyring.get_password("freebox_dev_opener", "main")
    challenge = None

    if app_token is None:
        (app_token, track_id) = create_authorization()

        # wait for the user to validate the authorization
        print("You need to accept the authorization on the freebox modem")
        print("Press enter when the authorization is validated")
        input()

        # finish the authorization
        challenge = finish_authorization(track_id)

        # save the authorization
        keyring.set_password("freebox_dev_opener", "main", app_token)

    else:
        challenge = get_challenge()

    password = hmac.new(app_token.encode('utf-8'), challenge.encode('utf-8'), hashlib.sha1).hexdigest()

    # retrieve session token
    session_token = fetch_session_token(password)

    return session_token


